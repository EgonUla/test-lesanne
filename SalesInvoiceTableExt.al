tableextension 50127 "Sales Invoice Extension" extends "Sales Invoice Line"
{
    fields
    {
        field(50125; "Item Group Code"; Code[20])
        {
            Caption = 'Item Group Code';
            DataClassification = ToBeClassified;
            TableRelation = "Item Group".Code;

        }
    }
}