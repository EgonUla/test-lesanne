tableextension 50125 "Sales Extension" extends "Sales Line"
{
    fields
    {
        field(50125; "Item Group Code"; Code[20])
        {
            Caption = 'Item Group Code';
            DataClassification = ToBeClassified;
            TableRelation = "Item Group".Code;

        }
    }
}