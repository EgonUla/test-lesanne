pageextension 50129 "Item Ledger Entries Page Extension" extends "Item Ledger Entries"
{
    layout
    {
        addafter(Description)
        {
            field("Item Group Code"; "Item Group Code")
            {
                ApplicationArea = All;
            }
        }
    }
}