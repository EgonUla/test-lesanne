pageextension 50126 "Sales Card Extension" extends "Sales Order Subform"
{
    layout
    {
        addafter("No.")
        {
            field("Item Group Code"; "Item Group Code")
            {
                ApplicationArea = All;

            }
        }
    }
}