tableextension 50128 "Item Ledger Entry Table Extension" extends "Item Ledger Entry"
{
    fields
    {
        field(50125; "Item Group Code"; Code[20])
        {
            Caption = 'Item Group Code';
            DataClassification = ToBeClassified;
            TableRelation = "Item Group".Code;

        }
    }
}