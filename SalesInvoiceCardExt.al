pageextension 50128 "Sales Invoice Card Extension" extends "Posted Sales Invoice Subform"
{
    layout
    {
        addafter("No.")
        {
            field("Item Group Code"; "Item Group Code")
            {
                ApplicationArea = All;
            }
        }
    }
}