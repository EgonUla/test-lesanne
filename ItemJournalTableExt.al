tableextension 50130 "Item journal Line Extension" extends "Item Journal Line"
{
    fields
    {
        field(50125; "Item Group Code"; Code[20])
        {
            Caption = 'Item Group Code';
            DataClassification = ToBeClassified;
            TableRelation = "Item Group".Code;

        }
    }
}