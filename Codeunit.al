codeunit 50130 MyCodeunit
{

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterValidateEvent', 'No.', false, false)]
    local procedure MyProcedur(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    var
        item: Record Item;
    begin

        If item.Get(Rec."No.") then begin
            Rec.Validate("Item Group Code", item."Item Group Code");
        end;

    end;


    [EventSubscriber(ObjectType::Codeunit, 80, 'OnAfterPostItemJnlLineBeforePost', '', false, false)]
    local procedure MyProcedu(var ItemJournalLine: Record "Item Journal Line"; SalesLine: Record "Sales Line")
    var


    begin

        ItemJournalLine."Item Group Code" := SalesLine."Item Group Code";

    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnAfterInitItemLedgEntry', '', false, false)]
    local procedure MyProce(var NewItemLedgEntry: Record "Item Ledger Entry"; ItemJournalLine: Record "Item Journal Line"; var ItemLedgEntryNo: Integer)
    var


    begin

        NewItemLedgEntry."Item Group Code" := ItemJournalLine."Item Group Code";

    end;


}

