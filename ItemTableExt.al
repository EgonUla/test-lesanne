tableextension 50123 "Item Extension" extends Item
{
    fields
    {
        field(50123; "Item Group Code"; Code[20])
        {
            Caption = 'Item Group Code';
            DataClassification = ToBeClassified;
            TableRelation = "Item Group".Code;

        }
    }


}